using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
//Handles Winning and losing, and the UI coming with it.
public class WinAndLose : MonoBehaviour
{
    public Text ENDtext;
    public void Win()
    {
        float TimeTaken = Time.timeSinceLevelLoad;
        ENDtext.text = "You Win! Press LMB to go back to main menu";
        GameObject.Find("Player").GetComponent<Playermovement>().CanMove = false;
        Cursor.visible = true;

        if (Input.GetButton("Fire1")) {

            SceneManager.LoadScene("StartMenu");

        } 
    }

    public void Lose()
    {
        ENDtext.text = "You Lose... Press LMB to go back to main menu to try again";
        GameObject.Find("Player").GetComponent<Playermovement>().CanMove = false;
        Cursor.visible = true;
        if (Input.GetButton("Fire1"))
        {
            SceneManager.LoadScene("StartMenu");

        }
    }

}