using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeartAnimController : MonoBehaviour
{
    public Animator Default;
    public MeshRenderer Heart;
    public float Wait = 0;
    private void OnTriggerEnter(Collider other)
    {
        GameObject.Find("Stats").GetComponent<TextHandler>().Health += 10;
        Heart.GetComponent<MeshRenderer>();
        Heart.enabled = !Heart.enabled;
        Default.Play("New State");

        Wait += 120;
      
    }

    private void Update()
    {
        if (Wait < 0)
        {
            Heart.enabled = Heart.enabled;            
           Default.Play("HeartAnim");
            
        }

        Wait -= Time.deltaTime;
    }
}
