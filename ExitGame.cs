using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitGame : MonoBehaviour
{

    public void ExitGameEXE()
    {
        Application.Quit();
        Debug.Log("Application Quit");
    }
}

//Handles the Exit Game button on the start screen. Only necessary for the downloaded version.