using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

//Main Menu Loadgame button
public class LoadGame : MonoBehaviour
{
    public Text Buttontext;
    public bool DidStartLoadingNewLevel;


    void LoadGameInitiate()

    {
        Debug.Log("Button Clicked");
        Buttontext.text = ("Loading...");

        if (DidStartLoadingNewLevel == false)
        {
            SceneManager.LoadScene(1);
            DidStartLoadingNewLevel = true;
        }

        
    }
}
