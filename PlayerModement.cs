using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Playermovement : MonoBehaviour
{
    public float PX;
    public float PY;
    public Transform Playerpos;
    public GameObject Fish;
    public float MovSpeed = 10f;
    public bool CanMove;


    private const float MAXY = 70f;
    private const float MINY = -70f;

    void Start()
    {
        Playerpos = transform;
        CanMove = true;
        Cursor.visible = false;
    }

    private void Update()
    {
        PX += Input.GetAxis("Mouse X");
        PY += Input.GetAxis("Mouse Y");
        PY = Mathf.Clamp(PY, MINY, MAXY);

    }

    private void LateUpdate()
    {
        Quaternion Rotation = Quaternion.Euler (PY, PX - 180, 0);
        Playerpos.rotation = Rotation;

        if (Input.GetButton("Vertical") && (CanMove == true))
        {
            Vector3 movement = Playerpos.transform.rotation * Vector3.back * MovSpeed;
            Playerpos.transform.position += movement * Time.deltaTime;

        }


    }
}
