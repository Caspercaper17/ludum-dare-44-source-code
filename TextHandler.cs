using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextHandler : MonoBehaviour
{
    
    public int Strength = 0;
    public int Health = 0;
    public Text UItext;
    public GameObject EndText;

    void Start()
    {
        
    }


    void Update()
    {
        UItext.text = "Strength: " + Strength + " " + " Health: " + Health; 
        if (Strength >= 100)
        {
            EndText.GetComponent<WinAndLose>().Win();
        }
        if (Health < 1)
        {
            EndText.GetComponent<WinAndLose>().Lose();
        }
    }
}
