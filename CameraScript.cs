using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{

    public Transform Lookat;
    public Transform CameraPos;

    public Camera Cam;
    public float distance = 15f;
    public float camheight = 5f;
    public float X = 0;
    public float Y = 0;


    private const float MAXY = 70f;
    private const float MINY = -70f;


    private void Update()
    {
        X += Input.GetAxis("Mouse X");
        Y += Input.GetAxis("Mouse Y");
        Y = Mathf.Clamp(Y, MINY, MAXY);

    }

    void Start()
    {
        CameraPos = transform;
        Cam = Camera.main;
    }

    private void LateUpdate()
    {
        Vector3 Direction = new Vector3(0, +camheight, -distance);
        Quaternion rotation = Quaternion.Euler(-Y, X, 0);
        CameraPos.LookAt(Lookat.position);
        CameraPos.position = Lookat.position + rotation * Direction;
    }


