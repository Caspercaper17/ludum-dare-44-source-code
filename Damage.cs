using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//Handles damage
public class Damage : MonoBehaviour
{
    public int DamageI;
    public GameObject DamageOverlay;
  


    void Start()
    {
        var SphereCol = gameObject.GetComponent<SphereCollider>();
        var BoxCol = gameObject.GetComponent<BoxCollider>();
    }

    private void OnTriggerEnter(Collider Fish)
    {
        GameObject.Find("Stats").GetComponent<TextHandler>().Health -= DamageI;
        DamageOverlay.SetActive(true);

    }
    private void OnTriggerExit(Collider Fish)
    {
        DamageOverlay.SetActive(false);
    }
}
