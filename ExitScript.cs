using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//This script handles exiting the game in the main scene with the esc button.

public class ExitScript : MonoBehaviour
{
    public Text ExitText;
    public bool ExitGameScreen;

    void Start()
    {
        ExitGameScreen = false;
    }


    void Update()
    {
        if (Input.GetButton("Cancel"))
        {
            ExitGameScreen = true;
            GameObject.Find("Player").GetComponent<Playermovement>().CanMove = false;
            ExitText.text = ("Are you sure you want to exit? LMB to confirm, RMB to cancel");
            Cursor.visible = true;
        }




        if ((ExitGameScreen == true) && Input.GetButton("Fire1"))
            {
            ExitGameScreen = false;
            Application.Quit();
            ExitText.text = ("Application Quited, If not, please press Alt+f4 :D");
            Debug.Log("Application Quited");
            }

            if (Input.GetButton("Fire2") && (ExitGameScreen == true))
            {
            GameObject.Find("Player").GetComponent<Playermovement>().CanMove = true;
            ExitText.text = (" ");
                ExitGameScreen = false;
            Cursor.visible = false;
        }
        

    }

}
